import React, { useEffect, useState } from 'react';

function PresentationForm(props) {
    const [formData, setFormData] = useState({
        presenter_name: '',
        presenter_email: '',
        company_name: '',
        title: '',
        synopsis: '',
        conference: '',
    })
    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }
    const [conferences, setConferences] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setConferences(data.conferences)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.presenter_name = formData.presenter_name;
        data.presenter_email = formData.presenter_email;
        data.company_name = formData.company_name;
        data.title = formData.title;
        data.synopsis = formData.synopsis;
        data.conference = formData.conference;
        console.log(data);

        const presenterUrl = 'http://localhost:8000'+formData.conference+'presentations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presenterUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            setFormData({
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            })
        }
    }
return(

    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
        <h1>Create a new presentation</h1>
        <form onSubmit={handleSubmit} id="create-presentation-form">
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
            <label htmlFor="presenter_name">Name</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
            <label htmlFor="Presenter email">Presenter email</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
            <label htmlFor="company_name">Company name</label>
        </div>
        <div className="mb-3">
            <input onChange={handleFormChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
            <label htmlFor="title">Title</label>
        </div>
        <div className="mb-3">
            <label htmlFor="synopsis">Synopsis</label>
            <textarea onChange={handleFormChange} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
        </div>
        <div className="mb-3">
            <select onChange={handleFormChange} required name="conference" id="conference" className="form-select">
                <option value="">Choose a conference</option>
                {conferences.map((conference) => {
                    return (
                        < option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                    )
                })}
            </select>
        </div>
        <button className="btn btn-primary">Create</button>
        </form>
    </div>
    </div>
</div>
)
}

export default PresentationForm;
