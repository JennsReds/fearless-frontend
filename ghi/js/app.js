function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class="col-4">
        <div class="card h-100 shadow">
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                <small class="text-muted">${start} - ${end}</small>
            </div>
        </div>
    </div>
    `;
}
function errorMessage(){
    return `
    <div class="alert alert-dark" role="alert">
        There is a problem with your request!
    </div>
    `;
}



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error ('Response not ok');
      } else {
        const data = await response.json();


        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const starts = details.conference.starts
            const start = new Date(starts).toLocaleDateString()
            const ends = details.conference.ends
            const end = new Date(ends).toLocaleDateString()


            const location = details.conference.location.name;

            const html = createCard(name, description, pictureUrl, start, end, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        const html = errorMessage();
        const column = document.querySelector(".row");
        column.innerHTML = html;
        console.log("There is a problem in your request!");

    }

  });
